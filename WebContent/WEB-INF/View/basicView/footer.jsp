<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!-- footer부분 -->
<footer class="footer-wrapper navbar-fixed-bottom">
	<div class="container">
		<div class="row">
			<div class="span3">
				<h4>Dogether</h4>
			</div>
		</div>
	</div>

	<div class="footer-bottom-wrapper">
		<div class="container">
			<div class="row">

				<div class="span6">
					<p>Copyright &copy; 2014 Dogether
				</div>
			</div>
		</div>
	</div>
</footer>