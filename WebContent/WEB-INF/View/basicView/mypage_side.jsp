<%@ page language="java" contentType="text/html; charset=EUC-KR"
	pageEncoding="EUC-KR"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
</head>

<script type="text/javascript">

	$(function() {
		$(".subject").click(function() {
			$("#accordian ul ul").slideUp();
			if (!$(this).next().is(":visible")) {
				$(this).next().slideDown();
			}
			else {
				
			}
		});
		
		var memberCheckCode=${memberCheckCode};
		
		if(memberCheckCode==1){ // 제공자는1			
			$(".resume-li").remove();
		}else{
			$(".project-li").remove();
			$(".point-li").remove();
		};
		
	});
</script>

<div id="accordian">
	<ul class="list-group sidebar-nav-v1" id="sidebar-nav">
		<li class="list-group-item list-toggle info-li">
			<a href="/Fish/modify/myinfochange.do" data-parent="#sidebar-nav" class="subject ">
			<span class="fa fa-edit">&nbsp;&nbsp;&nbsp;기본정보 수정</span>
			</a>
		</li>
		<li class="list-group-item list-toggle resume-li">
			<a href="#" data-toggle="collapse" data-parent="#sidebar-nav" class="subject">
			<span class="fa fa-file-text">&nbsp;&nbsp;&nbsp;이력서 관리</span>
			</a>
			<ul>
				<li><a href="/Fish/write/resumeWrite.do">&nbsp;기본 이력 관리</a></li>
				<li><a href="/Fish/write/portfolioWrite.do">&nbsp;포트폴리오 관리</a></li>
			</ul>
		</li>
		<li class="list-group-item list-toggle project-li">
			<a href="#" data-toggle="collapse" data-parent="#sidebar-nav" class="subject">
			<span class="fa fa-floppy-o">&nbsp;&nbsp;&nbsp;프로젝트 관리</span>
		</a>
			<ul>
				<li><a href="/Fish/write/projectwrite.do">&nbsp;내 프로젝트 등록</a></li>
				<li><a href="/Fish/projectInfo/projectBeforeMatching.do">&nbsp;내가 등록한 프로젝트 보기</a></li><!-- 
				<li><a href="/Fish/projectInfo/projectOngoing.do">&nbsp;매칭 후 진행 중인 프로젝트 보기</a></li>
				<li><a href="/Fish/projectInfo/projectDone.do">&nbsp;완료된 프로젝트 보기</a></li> -->
				<li><a href="#">&nbsp;프로젝트 일정표</a></li>
			</ul>
		</li>
		
		<li class="list-group-item list-toggle matching-li">
			<a href="#" data-toggle="collapse" data-parent="#sidebar-nav" class="subject"> 
			<span class="fa fa-heart-o">&nbsp;&nbsp;&nbsp;매칭 관리</span>
		</a>
			<ul>
				<li><a href="/Fish/matchingInfoView/beforeMatchingInfo.do">&nbsp;진행 중인 매칭 목록</a></li>
				<li><a href="/Fish/matchingInfoView/endMatchingInfo.do">&nbsp;완료된 매칭 목록</a></li>
			</ul>
		</li>
		<li class="list-group-item list-toggle point-li">
			<a href="/Fish/managepoint.do" data-parent="#sidebar-nav" >
				<span class="fa fa-won">&nbsp;&nbsp;&nbsp;포인트 관리</span>
			</a>
		</li>
	</ul>
</div>



</html>








