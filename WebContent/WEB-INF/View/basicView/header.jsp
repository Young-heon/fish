<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!-- header부분 -->
<head>
<!-- =================
	meta tag
================= -->
<meta charset="utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />

<!-- =================
	title
================= -->
<title>Let's go Dogether!</title>

<!-- =================
	CSS
================= -->
<link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap-glyphicons.css" rel="stylesheet">
	
<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
	
<link href="/Fish/css/bootstrap.css" rel="stylesheet" type="text/css" >
	
<link href="/Fish/css/animate.css" rel="stylesheet" type="text/css">
	
<link href="/Fish/css/custom.css" rel="stylesheet" type="text/css">

<!-- =================
	FAVICON
====================== -->
<link rel="shortcut icon" href="image/favicon.ico" type="image/x-icon">
<link rel="icon" href="image/favicon.ico" type="image/x-icon">


<!-- =================
	JavaScript
================= -->

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script src="http://www.azurekyle.com/rnm/redesign/js/bootstrap.js"></script>
<script src="/Fish/js/jquery-ex.js"></script>

</head>

<header>
	<div class="navbar navbar-default navbar-fixed-top" role="navigation">
		<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse"
					data-target=".navbar-collapse">
					<span class="sr-only">Toggle navigation</span> <span
						class="icon-bar"></span> <span class="icon-bar"></span> <span
						class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="/Fish/index.do" onmouseover="document.getElementById('logo').src='/Fish/image/logo_2.png';" onmouseout="document.getElementById('logo').src='/Fish/image/logo_1.png';">
					<img src="/Fish/image/logo_1.png" id="logo" width="175px" height="60px"/>
				</a>
			</div>
			<div class="navbar-collapse collapse">
				<ul class="nav navbar-nav">
					<li class="active">
						<a href="/Fish/introduction.do">Dogether?</a>
					</li>
					<li class="active">
						<a href="/Fish/project.do" class="topMenu">프로젝트 고르기</a>
					</li>
					<li class="active">
						<a href="/Fish/freelancer.do" class="topMenu">프리랜서 고르기</a>
					</li>
				</ul>
				<ul class="nav navbar-nav navbar-right">
					<li><a href="">${memberName}님</a></li>
					<li><a href="/Fish/logout.do">로그아웃</a></li>
					<li><a href="/Fish/mypage.do">마이페이지</a></li>
				</ul>
			</div>
		</div>
	</div>

</header>