<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="ko">
<jsp:include page="../basicView/header.jsp" />

<script>
	$(function() {
		$(".matching-li").addClass('active');
	});
</script>

<body>
	<div class="wrapper">
		<div class="header">
		</div>
		<div class="container content">
			<div class="row">
				<div class="col-md-3">
					<jsp:include page="../basicView/mypage_side.jsp"/>
				</div>
				<div class="col-md-9">
					<table id="portfolioTitle">
						<tr>
							<th><span class="fa fa-angle-right">&nbsp;&nbsp;완료된 매칭 목록</span></th>
						</tr>
						<tr>
							<td>매칭이 완료된 프로젝트의 간략한 정보와 프리랜서 목록을 볼 수 있고, 프로젝트 상세 보기, 프리랜서 정보를 상세보기 할 수 있습니다. </td>
						</tr>
					</table>
					<br><br>
					
					<table id="portfolioTable">
						<tr>
							<th>
								프로젝트 제목
							</th>
							<td>
								법인양도양수 사이트 제작
							</td>
							<td width="80px">
								<img src="/Fish/image/ongoing.png" width="50px"/>
							</td>
						</tr>
						<tr>
							<td colspan="2" align="center">
								<span class='fa fa-won'> 예상금액 40000000원&nbsp;&nbsp;</span>
								<span class='glyphicon glyphicon-time'> 예상기간 45일&nbsp;&nbsp;</span>
								<span class='glyphicon glyphicon-map-marker'> 진행지역 서울&nbsp;&nbsp;</span>
								<span class='glyphicon glyphicon-calendar'> 등록일자 2014-08-01&nbsp;&nbsp;</span>
								<span class='glyphicon glyphicon-thumbs-up'> 총 3명 지원</span>
							</td>
							<td>
								<a href="#">상세보기</a>
							</td>
						</tr>
						<tr>
							<th rowspan="3">매칭된 프리랜서 목록</th>
							<td>spiyer</td>
							<td><a href="#">상세보기</a></td>
						</tr>
						<tr>
							<td>konon2001</td>
							<td><a href="#">상세보기</a></td>
						</tr>
						
					</table>
					
					<br><br>
					
					<table id="portfolioTable">
						<tr>
							<th>
								프로젝트 제목
							</th>
							<td>
								아이콘 세트 디자인
							</td>
							<td width="80px">
								<img src="/Fish/image/end.png" width="50px"/>
							</td>
						</tr>
						<tr>
							<td colspan="2" align="center">
								<span class='fa fa-won'> 예상금액 180000000원&nbsp;&nbsp;</span>
								<span class='glyphicon glyphicon-time'> 예상기간 90일&nbsp;&nbsp;</span>
								<span class='glyphicon glyphicon-map-marker'> 진행지역 부산&nbsp;&nbsp;</span>
								<span class='glyphicon glyphicon-calendar'> 등록일자 2014-02-01&nbsp;&nbsp;</span>
								<span class='glyphicon glyphicon-thumbs-up'> 총 7명 지원</span>
							</td>
							<td>
								<a href="#">상세보기</a>
							</td>
						</tr>
						<tr>
							<th rowspan="3">매칭된 프리랜서 목록</th>
							<td>dhrhdms</td>
							<td><a href="#">상세보기</a></td>
						</tr>
						<tr>
							<td>jypenter</td>
							<td><a href="#">상세보기</a></td>
						</tr>
						<tr>
							<td>lees2_s2</td>
							<td><a href="#">상세보기</a></td>
						</tr>
						
					</table>
					
				</div>
			</div>
		</div>
		<div class="footer">
			<jsp:include page="../basicView/footer.jsp" />
		</div>
	</div>
</body>
</html>