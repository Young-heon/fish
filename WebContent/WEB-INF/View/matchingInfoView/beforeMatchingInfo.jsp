<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="ko">
<jsp:include page="../basicView/header.jsp" />

<script>
	$(function() {
		function getProjectSubject(projectNo,id){
			$.ajax({
				url:"../projectDetail.do",
				type:"post",
				dataType:"json",
				data:"projectNo="+projectNo,
				success:function(result){
					$("#"+id).text(result.pv.projectSubject);
				},
				error:function(error){
					alert(error);
				}
			});
		}
		function getProjectDetail(projectNo,id){ //프로젝트세부정보를 가져온다.
			$.ajax({
				url:"../projectDetail.do",
				type:"post",
				dataType:"json",
				data:"projectNo="+projectNo,
				success:function(result){
					$("#"+id).html("<span class='fa fa-won'/>&nbsp;&nbsp;예상금액&nbsp;"+result.pv.projectAmount+"원&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;<span class='glyphicon glyphicon-time'/>&nbsp;&nbsp; 예상기간&nbsp;"+((result.pv.projectEndDate.substring(8,10)-result.pv.projectStartDate.substring(8,10))+"일&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;<span class='glyphicon glyphicon-map-marker'/>&nbsp;&nbsp; 진행지역&nbsp;"+result.pv.projectLocation+"&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;<span class='glyphicon glyphicon-calendar'/>&nbsp;&nbsp;등록일자&nbsp;"+result.pv.projectEnrollStart.substring(0,10)+"&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;<span class='glyphicon glyphicon-thumbs-up'/>&nbsp;&nbsp;총 " + result.pv.projectPeople + "명 지원"));
				},
				error:function(error){
					alert(error);
				}
			});
		}
		
		$(".matching-li").addClass('active');
		$.ajax({
			url:"projectOngoingList.do",
			type:"post",
			dataType:"json",
			data:"memberId=${memberId}",
			success:function(getData){
				var resultHtml="";
				getData.mepv.sort(function(a,b){
					var x = a.projectNo > b.projectNo ? -1: 1;
					return x;
				});
				 $(getData.mepv).each(function(index,item){
					var projectNum= item.projectNo; 
					var enrollUsers = item.enrollUser.split(',');
					//console.log(enrollUsers);
					resultHtml+='<table id="portfolioTable"><tr><th>프로젝트 제목</th><td id="projectSubject'+index+'" colspan="2">';
					getProjectSubject(projectNum,"projectSubject"+index);
					resultHtml+=projectNum;
					resultHtml+='</td></tr><tr><td id="projectInfo'+index+'" colspan="2"">예상금액, 예상기간, 진행지역, 등록일자, 지원자 수</td></tr>';
					getProjectDetail(projectNum,"projectInfo"+index);
					resultHtml+='<tr><th colspan="1">지원자 목록</th><td><span id="printAttend">';
					
					for(var i in enrollUsers){
						resultHtml+='<a href="#" id="userModal" value="'+enrollUsers[i] + '">' + enrollUsers[i] + '</a>&nbsp;&nbsp;&nbsp;';
					}
					resultHtml+='</span></td></tr>';
					resultHtml+='</tr></table><br/>';
					
				}); 
				$(".printList").html(resultHtml);

			},
			error:function(err){
				alert(err+"error발생");
			}
			
		});
		$(document).on("click","#userModal",function(){
			function getImageModal(memberId){
				$.ajax({
					url:"../developerImage.do",
					type:"post",
					dataType:"text",
					data:"memberId="+memberId,
					success:function(imageData){
						if(imageData==""){
							imageUrl="http://203.252.166.212/Fish/image/suzanne.png";
						}else{
							imageUrl="http://203.252.166.212"+imageData;
							//alert(imageData)
						}
						
						$("#modalImage").attr("src",imageUrl);
					},
					error:function(err){
						imageUrl=err;
					}
				});//ajax end
				//alert("END")		
		}  
			
		$.ajax({
			url:"../freeDetail.do",
			type:"post",
			dataType:"json",
			data:"memberId="+$(this).attr("value"),
			success:function(detailResult){
				
				var memberDetail="";
				getImageModal($(this).attr("value"));
				memberDetail+='<div class="modal-freelancerDetail-dialog contcustom modal-freelancerDetail-dialog-center">';
				memberDetail+='<div class="modal-content"><div class="modal-body">';
				memberDetail+='<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>';
				memberDetail+='<table class="freelancer_detail_table"><tr><td width="30%">';
				memberDetail+='<img id="modalImage" class="media-object img-circle" src="image/tom.png" width="150px" height="150px"> <!-- 프로필 이미지 --></td>';
				memberDetail+='<td width="70%"><h2 id="login-font">';
				memberDetail+=detailResult.memberuser;
				memberDetail+='</h2><p><span class="fa fa-check"></span>&nbsp;&nbsp;'+detailResult.athenMessage;
				
				memberDetail+='</p></td></tr><tr><td colspan="2"><table id="freelancerDetailTitle"><tr><th colspan="5">';
				memberDetail+='<span class="fa fa-angle-right">&nbsp;&nbsp;경력 사항</span></th></table>';
				memberDetail+='<table id="careerTable" cellspacing="0"><tr><th width="160px">회사 명</th>';
				memberDetail+='<th width="160px">재직 기간</th><th width="100px">직급</th><th width="100px">부서</th>';
				memberDetail+='</tr>';
				
				$(detailResult.career).each(function(index,carr){
					memberDetail+='<tr>';
					memberDetail+='<td width="160px">'+carr.careerCompany+'</td>';
					memberDetail+='<td width="160px">'+carr.careerPeriod+'</td>';
					memberDetail+='<td width="100px">'+carr.careerPosition+'</td>';
					memberDetail+='<td width="100px">'+carr.careerDepartment+'</td>';
					memberDetail+='</tr>';
				});
				
				memberDetail+='</table></td></tr><tr><td colspan="2"><table id="freelancerDetailTitle"><tr><th colspan="5">';
				memberDetail+='<span class="fa fa-angle-right">자격 사항</span></td></tr></table>';
				memberDetail+='<table id="certificationTable" cellspacing="0"><tr><th width="180px">자격사항 명</th>';
				memberDetail+='<th width="180px">발급 기관 </th><th width="180px">발급 일자</th></tr>';
				$(detailResult.certi).each(function(index,certifi){
					memberDetail+='<tr>';
					memberDetail+='<td width="180px">'+certifi.certificationName+'</td>';
					memberDetail+='<td width="180px">'+certifi.certificationCompany+'</td>';
					memberDetail+='<td width="180px">'+certifi.certificationDate+'</td>';
					memberDetail+='</tr>';
				});
				
				memberDetail+='</table></td></tr><tr><td colspan="2"><br><table id="freelancerDetailTitle"><tr>';
				memberDetail+='<th colspan="5"><span class="fa fa-angle-right">사용 가능한 프로그래밍 언어</span></td></tr></table>';
				memberDetail+='<table id="languageTable" cellspacing="0">';
				memberDetail+='<tr><th width="180px">언어명</th><th width="220px">활용 등급</th><th width="120px">사용시간</th></tr>';
				$(detailResult.lang).each(function(index,langs){
					memberDetail+='<tr>';
					memberDetail+='<td width="180px">'+langs.languageName+'</td>';
					memberDetail+='<td width="220px">'+langs.languageLevel+'</td>';
					memberDetail+='<td width="120px">'+langs.languagePeriod+'</td>';
					memberDetail+='</tr>';
				});
				
				memberDetail+='</table></td></tr><tr><td colspan="2"><br><table id="freelancerDetailTitle"><tr><th>';
				memberDetail+='<div class="fa fa-angle-right">자기 소개</div></td></tr><tr><td>';
				memberDetail+=detailResult.selfIntro;
				
				memberDetail+='</td></tr></table><br><table id="freelancerDetailTitle"><tr><th colspan="5">';
				memberDetail+='<div class="fa fa-angle-right">포트폴리오</div></td></tr></table></td></tr>';
				memberDetail+='</table></div></div></div>';
				$("#modalDetailUser").html(memberDetail);
				},
			
			error:function(err){
				alert(err+"error발생");	
			}
		});
		
		$("#modalDetailUser").modal('show');

		});
		
	});
</script>

<body>
	<div class="wrapper">
		<div class="header">
		</div>
		<div class="container content">
			<div class="row">
				<div class="col-md-3">
					<jsp:include page="../basicView/mypage_side.jsp"/>
				</div>
				<div class="col-md-9">
					<table id="portfolioTitle">
						<tr>
							<th><span class="fa fa-angle-right">&nbsp;&nbsp;진행 중인 매칭 목록</span></th>
						</tr>
						<tr><td>
						프로젝트 제공자에게는 내 프로젝트를 선택한 프리랜서의 목록이, 프리랜서에게는나를 선택한 제공자의 프로젝트 목록이 보여집니다.<br><br>
						</td></tr>
					</table>
					<br>
					<table id="portfolioTable" class="printList">
					</table>
					
				</div>
			</div>
			<div class="modal" id="modalDetailUser">


			</div>
			
		</div>
		<div class="footer">
			<jsp:include page="../basicView/footer.jsp" />
		</div>
	</div>
</body>
</html>