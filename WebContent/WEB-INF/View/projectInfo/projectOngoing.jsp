<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>

<!doctype html>
<html>
<head>

<jsp:include page="../basicView/header.jsp" />

<title>Ongoing Project!!</title>

<script type="text/javascript">
	$(function() {
		
		var memberCheckCode=${memberCheckCode};
		if(memberCheckCode==1){ // 제공자는1			
			
		}else{
			$("#point").hide();
		}	
		
		$(".subject").click(function() {
			$("#accordian ul ul").slideUp();
			if (!$(this).next().is(":visible")) {
				$(this).next().slideD own();
			}
		});

		$(".project-li").addClass('active');

	});
</script>

<script>
	$(document).ready(function() {
		var text="";
		$.ajax({
			url : "userOngoingProject.do",
			type : "post",
			dataType : "text",
			data : "memberId=${memberId}",
			success : function(result) {
			
				if (result.list == null) { //
					
					text+="<div><br><br><br><br><br><br><br><br>";
					text+="<h1>      현재 진행중인 프로젝트가 없습니다.</h1></div>";			
					$("#printProject").html(text);
				}
			},
			error : function(err) {
				alert(err + "데이터 받아오기 실패");
			}

		});
	});
</script>
<body>
	<div class="wrapper">
		<div class="header">
		</div>
		<div class="container content">
			<div class="row">
				<div class="col-md-3">
					<jsp:include page="../basicView/mypage_side.jsp"/>
				</div>
				<div class="col-md-9">
					<section id="card">
						<div class="row" id="printProject">
							<div class="col-sm-12 scrollpoint sp-effect active">
								<div class="media">
									<div class="media-body" id="">
										<h4 class="media-heading">프로젝트 제목</h4>
										<h6>
											<p>
												<span class="fa fa-won"></span>&nbsp;&nbsp;예상금액&nbsp;원&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
												<span class="glyphicon glyphicon-time"></span>&nbsp;&nbsp;예상기간&nbsp;일&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
												<span class="glyphicon glyphicon-map-marker"></span>&nbsp;&nbsp;진행지역&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
												<span class="glyphicon glyphicon-calendar"></span>&nbsp;&nbsp;등록일자&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
												<span class="glyphicon glyphicon-thumbs-up"></span>&nbsp;&nbsp;총&nbsp;명
												지원&nbsp;&nbsp;&nbsp;
											</p>
											<p>프로젝트 상세 설명 부분</p>
											<p>요구기술 OOO, OO</p>
										</h6>
									</div>
								</div>
							</div>


						</div>
					</section>
				</div>
			</div>
		</div>
		<div class="footer">
			<jsp:include page="../basicView/footer.jsp" />
		</div>
	</div>
</body>
</html>