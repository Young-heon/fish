<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>

<!doctype html>
<html>
<head>

	<jsp:include page="../basicView/header.jsp" />

<title>Ongoing Project!!</title>

<script type="text/javascript">
	$(function() {
		
		var memberCheckCode=${memberCheckCode};
		if(memberCheckCode==1){ // 제공자는1			
			
		}else{
			$("#point").hide();
		}	
		
		$(".project-li").addClass('active');
	});
</script>

<script>
	$(document).ready(function() {
		$.ajax({
			url : "userOngoingProject.do",
			type : "post",
			dataType : "text",
			data : "memberId=${memberId}",
			success : function(result) {
				alert(result);
				if (result === null) {
					alert("현재 완료된 프로젝트가 없습니다.");
				}
			},
			error : function(err) {
				alert(err + "데이터 받아오기 실패");
			}

		});
	});
</script>
<body>
	<div class="wrapper">
		<div class="header">
		</div>
		<div class="container content">
			<div class="row">
				<div class="col-md-3">
					<jsp:include page="../basicView/mypage_side.jsp"/>
				</div>
				<div class="col-md-9">
					준비중인 페이지입니다.
				</div>
			</div>
		</div>
		<div class="footer">
			<jsp:include page="../basicView/footer.jsp" />
		</div>
	</div>
</body>
</html>