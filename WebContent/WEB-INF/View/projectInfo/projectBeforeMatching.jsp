<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>

<!doctype html>
<html>
<head>

<jsp:include page="../basicView/header.jsp" />

<script src="../js/jquery/1.11.1/jquery.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
<link href="../css/bootstrap.css" rel="stylesheet" type="text/css" charset="utf-8">

<title>Before Project!!</title>

<script type="text/javascript">
	$(function() {
		$(".project-li").addClass('active');
	});
</script>

<script>
	$(document).ready(function() {
			$.ajax({
				url:"supplyBeforeProject.do",
				type:"post",
				dataType:"json",
				data:"memberId=${memberId}",
				success:function(getData){
					
					var table="";
					getData.list.sort(function(a,b){
						var x = a.projectNo > b.projectNo ?-1:1;
						return x;
					});
					$(getData.list).each(function(index,item){
						var year = item.projectStartDate.substring(0,4);
						var year2 =  item.projectEndDate.substring(0,4);
						var month =  item.projectStartDate.substring(5,7);
						var month2 =  item.projectEndDate.substring(5,7);
						var day =  item.projectStartDate.substring(8,10);
						var day2 = item.projectEndDate.substring(8,10);
						var date1 = new Date(year,month,day);
						var date2 = new Date(year2,month2,day2);
						var gap = (date2-date1)/(1000*60*60*24);
						table += "<tr>";
						table += "<th>프로젝트 번호</th>";
						table += "<td colspan='2'>" + item.projectNo + "</td>";
						table += "</tr>";
						table += "<tr>";
						table += "<th>프로젝트 제목</th>";
						table += "<td colspan='2'>" + item.projectSubject + "</td>";
						table += "</tr>";
						table += "<tr>";
						table += "<th>등록일자</th>";
						table += "<td colspan='2'>" + item.projectEnrollStart.substr(0,10) + "</td>";
						table += "</tr>";
						table += "<tr>";
						table += "<th>모집 마감일자</th>";
						table += "<td colspan='2'>" + item.projectEnrollEnd.substr(0,10) + "</td>";
						table += "</tr>";
						table += "<tr>";
						table += "<th>기본 정보</th>";
						table += "<td colspan='2'>";
						table += "<span class='fa fa-won'/>&nbsp;&nbsp;예상금액&nbsp;" + item.projectAmount + "원&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
						table += "<span class='glyphicon glyphicon-time'/>&nbsp;&nbsp;예상기간&nbsp;" + gap + "일&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
						table += "<span class='glyphicon glyphicon-map-marker'/>&nbsp;&nbsp;진행지역&nbsp;" + item.projectLocation + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
						table += "<span class='glyphicon glyphicon-calendar'/>&nbsp;&nbsp;등록일자&nbsp;" + item.projectEnrollStart.substr(0,10) + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
						table += "<span class='glyphicon glyphicon-thumbs-up'/>&nbsp;&nbsp;총&nbsp" + item.projectPeople + "명 지원";
						table += "</td>";
						table += "</tr>";
						table += "<tr>";
						table += "<th>세부 사항</th>";
						table += "<td colspan='2'>" + item.projectDetail + "</td>";
						table += "</tr>";
						table += "<tr>";
						table += "<th>요구 기술</th>";
						table += "<td colspan='2'>" + item.projectTech + "</td>";
						table += "</tr>";
						table += "<tr><td><br><br></td></tr>";
					});
					$("#portfolioTable").html(table);
				},
				error:function(err){
					alert("데이터가 받아오기 실패");
				}
			});
});
</script>

</head>
<body>

	<div class="wrapper">
		<div class="header">
		</div>
		<div class="container content">
			<div class="row">
				<div class="col-md-3">
					<jsp:include page="../basicView/mypage_side.jsp" />
				</div>
				<div class="col-md-9">
					<section id="card">
						<div class="row" id="printProject">
							<table id="portfolioTitle">
								<tr>
									<th><span class="fa fa-angle-right">&nbsp;&nbsp;내가
											등록한 프로젝트 목록</span></th>
								</tr>
							</table>
							<br><br>
							<table id="portfolioTable" cellspacing="0">
								<tr>
									<td><br></td>
								</tr>
							</table>
							<table id="portfolioTable" cellspacing="0">
								<tr>
								</tr>
							</table>
							<br> <br> <br> <br> <br>
						</div>
					</section>
				</div>
			</div>

			<!-- modal 개발자 상세페이지 -->
			<div class="modal" id="modal_applicant">
				<div class="modal-applicant-dialog contcustom modal-applicant-dialog-center">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal"
								aria-hidden="true">&times;</button>
							&nbsp;&nbsp;&nbsp;<span class="fa fa-users bigicon"></span>
							<h2 id="login-font">지원자 목록</h2>
						</div>
						<div class="modal-body">
							<table class="applicant_table">
								<tr>
									<th>지원자 아이디</th>
									<td>아이디 뜨는 공간</td>
								</tr>
								<tr>
									<th>지원자 경력</th>
									<td>경력 뜨는 공간</td>
								</tr>
								<tr>
									<th>지원자 보유기술</th>
									<td>보유기술 뜨는 공간</td>
								</tr>
								<tr>
									<th>지원자 자격증</th>
									<td>지원자 자격사항 뜨는 공간</td>
								</tr>
								<tr>
									<th>지원자 포트폴리오</th>
									<td>포트폴리오 뜨는 공간</td>
								</tr>
								<tr>
									<th>지원자 자기 소개</th>
									<td>자기소개 띄우는 공간</td>
								</tr>
							</table>
							</div>
						</div>
					</div>
					<!-- /.modal-content -->
				</div>
				<!-- /.modal-dialog -->
			</div>
			<!-- /.modal -->

	</div>
	<div class="footer">
		<jsp:include page="../basicView/footer.jsp" />
	</div>
</body>
</html>