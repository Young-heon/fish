<%@ page language="java" contentType="text/html; charset=EUC-KR"
    pageEncoding="EUC-KR"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<jsp:include page="basicView/header.jsp" />
<meta http-equiv="Content-Type" content="text/html; charset=EUC-KR">
 <!-- Custom CSS -->
    <link href="http://startbootstrap.com/templates/agency/css/agency.css" rel="stylesheet">
<title>Insert title here</title>
</head>
<body>
  <!-- About Section -->
    <section id="about">
        <div class="container">

			<div id="sidebar-wrapper">
				<jsp:include page="basicView/mypage_side.jsp" />
			</div>
			
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2 class="section-heading">완료된 프로젝트 </h2>
                    <h3 class="section-subheading text-muted">${memberId}님의 프로젝트 기록입니다.</h3>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <ul class="timeline">
                        <li>
                            <div class="timeline-image">
                                <img class="img-circle img-responsive" src="image/jonathan.png" alt="">
                            </div>
                            <div class="timeline-panel">
                                <div class="timeline-heading">
                                    <h4>2009-2011</h4>
                                    <h4 class="subheading">빠르게 DB모델링 해주실분 구합니다.</h4>
                                </div>
                                <div class="timeline-body">
                                    <p class="text-muted">현재 DB는 그저 텍스트 문서 저장하는 용도로 사용하고 입습니다.착실하게 모델링 해주
                                    실분 구해요 ㅠㅠ급구입니다! 어서 신청해주세요!!!</p>
                                    <p class="text-muted"> 프로젝트기간: 2014.07.22-2014.08.21</p>
                                    <p class="test-muted"> 요구기술: Oracle DB</p>
                                </div>
                            </div>
                        </li>
                        <li class="timeline-inverted">
                            <div class="timeline-image">
                                <img class="img-circle img-responsive" src="image/b.jpg" alt="">
                            </div>
                            <div class="timeline-panel">
                                <div class="timeline-heading">
                                    <h4>March 2011</h4>
                                    <h4 class="subheading">개발자구함</h4>
                                </div>
                                <div class="timeline-body">
                                    <p class="text-muted">건국대에서 진행하는 DB취업아카데미 프로젝트로 도와주실분 구합니다!!!</p>
                                    <p class="text-muted">요구기술: Spring Java</p>
                            </div>
                        </li>
                        <li>
                            <div class="timeline-image">
                                <img class="img-circle img-responsive" src="image/a.jpg" alt="">
                            </div>
                            <div class="timeline-panel">
                                <div class="timeline-heading">
                                    <h4>December 2012</h4>
                                    <h4 class="subheading">DB모델러 구합니다!!!</h4>
                                </div>
                                <div class="timeline-body">
                                    <p class="text-muted">아직 구하지 못했습니다 일주일남았어요 신청해주세요!</p>
                                    <p class="text-muted">요구기술 Oracle DB</p>
                                </div>
                            </div>
                        </li>
                        
                        <li class="timeline-inverted">
                            <div class="timeline-image">
                                <h4>I've 
                                    <br>Done
                                    <br>These Projects!</h4>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
    
    <jsp:include page="basicView/footer.jsp" />

</body>
</html>