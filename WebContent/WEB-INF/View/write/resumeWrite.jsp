<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="ko">
<head>
<style>
</style>

<jsp:include page="../basicView/header.jsp" />

<script src="lib/jquery-1.11.1.min.js"></script>
<!--Menu End-->

<script type="text/javascript">
	$(function() {
		
		var memberCheckCode=${memberCheckCode};
		if(memberCheckCode==1){ // 제공자는1			
			
		
		}else if(memberCheckCode==2){ //개발자 이다.
			$("#designTool").hide();
			$("#point").hide();
		}else if(memberCheckCode==3){ //디자이너이다.
			$("#programingTool").hide();
			$("#point").hide();
		}	
		
		$(".resume-li").addClass('active');
	});

	$(function() {
		// 경력부분!!
		$("#careerBtn").click(function() {
			$.ajax({
				url : "careerInsert.do",
				type : "post",
				dataType : "text",//원래 텍스트 
				data : $("#careerForm").serialize()+"&memberId=${memberId}",//폼에 있는 모든 데이터 전송
				success : function(data) {
					//alert(data[0])
					if (data > 0) {
						alert("경력이 추가되었습니다.");
						//텍스트박스 모두 지우기
						$("input[type=text]").val("");
						//모든 레코드 검색하는 함수호출		
						careerData();
					} else {
						alert("경력 추가 실패");
					}
				},
				error : function(data) {
					alert(data + "error발생");
				}
			});//ajax끝
		});

		function careerData() {
			$.ajax({
				url : "careerSelect.do",//서버에 요청할 정보
				type: "post",
				data:"memberId=${memberId}",
				dataType : "json",//결과 데이터 타입
				success : function(data) {//성공했을때
				var table = "";
				//기존에 있는 테이블 첫행만 빼고 지우기
				$("#careerTable tr:gt(0)").remove();
				$(data.list).each(function(index, item) {
					table += "<tr>";
					table += "<td>"+ item.careerCompany+ "</td>";
					table += "<td>"+ item.careerPeriod+ "</td>";
					table += "<td>"+ item.careerPosition+ "</td>";
					table += "<td>"+ item.careerDepartment+ "</td>";
					table += "<td><button type='button' class='addBtn' value='삭제' id='careerDel' name='"+item.careerNo+ "'><span class='fa fa-trash-o'></span></input></td>";
					table += "</tr>";
					});//반복문끝

							//테이블에 추가한다.
					$("#careerTable tr:eq(0)").after(table);
				},
				error : function(data) {
						alert(data+"error발생");
					}

				});//ajax 끝		

		}//careerData끝

		//데이터삭제
		//on함수는 동적으로 생성된 요소에 이벤트 적용
		$(document).on("click", "#careerDel", function() {
			$.ajax({
				url : "careerDelete.do",
				type : "post",
				dataType : "text",
				data : "careerNo=" + $(this).attr("name"),
				success : function(data) {
					alert("경력 삭제.");
					careerData();
				},
				error : function(data) {
					alert("경력 삭제");
				}
			})

		})
		careerData();

		//자격증 추가
		$("#certificationBtn").click(
				function() {
					$.ajax({
						url : "certificationInsert.do",
						type : "post",
						dataType : "text",
						data : $("#certificationForm").serialize()
								+ "&memberId=${memberId}",//폼에 있는 모든 데이터 전송
						success : function(data) {

							if (data > 0) {
								alert("자격증 추가되었습니다.");
								//텍스트박스 모두 지우기
								$("input[type=text]").val("");
								//모든 레코드 검색하는 함수호출		
								certificationData();
							} else {
								alert("자격증 추가 실패");
							}
						},
						error : function(data) {
							alert(data + "error발생");
						}

					});//ajax끝
				})

		function certificationData() {
			$
					.ajax({
						url : "certificationSelect.do",//서버에 요청할 정보
						type : "post",
						dataType : "json",//결과 데이터 타입
						data:"memberId=${memberId}",
						success : function(data) {//성공했을때
							var table = "";

							//기존에 있는 테이블 첫행만 빼고 지우기
							$("#certificationTable tr:gt(0)").remove();

							$(data.list)
									.each(
											function(index, item) {
												table += "<tr>";
												table += "<td>"
														+ item.certificationName
														+ "</td>";
												table += "<td>"
														+ item.certificationCompany
														+ "</td>";
												table += "<td>"
														+ item.certificationDate
														+ "</td>";
												table += "<td><button type='button' class='addBtn' value='삭제' id='certificationDel' name='"+item.certificationNo+ "'><span class='fa fa-trash-o'></span></input></td>";
												table += "</tr>";
											})//반복문끝

							//테이블에 추가한다.
							$("#certificationTable tr:eq(0)").after(table);
						},
						error : function(data) {
							//alert(data+"error발생");
						}

					});//ajax 끝		

		}//certificationData끝

		//자격증 삭제
		$(document).on("click", "#certificationDel", function() {
			$.ajax({
				url : "certificationDelete.do",
				type : "post",
				dataType : "text",
				data : "certificationNo=" + $(this).attr("name"),
				success : function(data) {
					alert("삭제되었습니다.");
					certificationData();
				},
				error : function(data) {
					alert("삭제실패");
				}
			}); //ajax끝

		})
		certificationData();
		//	})

		// 사용언어등록 부분!!
		$("#languageBtn").click(
				function() {
					$.ajax({
						url : "languageInsert.do",
						type : "post",
						dataType : "text",//원래 텍스트 
						data : $("#languageForm").serialize()
								+ "&memberId=${memberId}",//폼에 있는 모든 데이터 전송
						success : function(data) {
							//alert(data[0])
							if (data > 0) {
								alert("사용언어가 추가되었습니다.");
								//텍스트박스 모두 지우기
								$("input[type=text]").val("");
								//모든 레코드 검색하는 함수호출		
								languageData();
							} else {
								alert("사용언어 추가 실패");
							}
						},
						error : function(data) {
							alert(data + "error발생");
						}
					});//ajax끝
				})

		function languageData() {
			$
					.ajax({
						url : "languageSelect.do",//서버에 요청할 정보
						type : "post",
						dataType : "json",//결과 데이터 타입
						data:"memberId=${memberId}",
						success : function(data) {//성공했을때
							var table = "";

							//기존에 있는 테이블 첫행만 빼고 지우기
							$("#languageTable tr:gt(0)").remove();

							$(data.list)
									.each(
											function(index, item) {
												table += "<tr>";
												table += "<td>"
														+ item.languageName
														+ "</td>";
												table += "<td> 레벨:"
														+ item.languageLevel
														+ "</td>";
												table += "<td>"
														+ item.languagePeriod
														+ "</td>";
												table += "<td><button type='button' class='addBtn' value='삭제' id='languageDel' name='"+item.languageNo+ "'><span class='fa fa-trash-o'></span></input></td>";
												table += "</tr>";
											})//반복문끝

							//테이블에 추가한다.
							$("#languageTable tr:eq(0)").after(table);
						},
						error : function(data) {
							//alert(data+"error발생");
						}

					});//ajax 끝		

		}//certificationData끝

		//사용언어 삭제
		$(document).on("click", "#languageDel", function() {

			$.ajax({
				url : "languageDelete.do",
				type : "post",
				dataType : "text",
				data : "languageNo=" + $(this).attr("name"),
				success : function(data) {
					alert("삭제되었습니다.");
					languageData();
				},
				error : function(data) {
					alert("삭제실패");
				}
			}); //ajax끝

		})
		languageData();

		// 사용툴등록 부분!!
		$("#toolBtn").click(function() {
			$.ajax({
				url : "toolInsert.do",
				type : "post",
				dataType : "text",//원래 텍스트 
				data : $("#toolForm").serialize() + "&memberId=${memberId}",//폼에 있는 모든 데이터 전송
				success : function(data) {
					//alert(data[0])
					if (data > 0) {
						alert("사용언어가 추가되었습니다.");
						//텍스트박스 모두 지우기
						$("input[type=text]").val("");
						//모든 레코드 검색하는 함수호출		
						toolData();
					} else {
						alert("사용언어 추가 실패");
					}
				},
				error : function(data) {
					alert(data + "error발생");
				}

			});//ajax끝
		})

		function toolData() {
			$
					.ajax({
						url : "toolSelect.do",//서버에 요청할 정보
						type : "post",
						dataType : "json",//결과 데이터 타입
						data:"memberId=${memberId}",
						success : function(data) {//성공했을때
							var table = "";

							//기존에 있는 테이블 첫행만 빼고 지우기
							$("#toolTable tr:gt(0)").remove();

							$(data.list)
									.each(
											function(index, item) {
												table += "<tr>";
												table += "<td>" + item.toolName
														+ "</td>";
												table += "<td> 레벨:"
														+ item.toolLevel
														+ "</td>";
												table += "<td>"
														+ item.toolPeriod
														+ "</td>";
												table += "<td><button type='button' class='addBtn' value='삭제' id='toolDel' name='"+item.toolNo+ "'><span class='fa fa-trash-o'></span></input></td>";
												table += "</tr>";
											})//반복문끝

							//테이블에 추가한다.
							$("#toolTable tr:eq(0)").after(table);
						},
						error : function(data) {
							//alert(data+"error발생");
						}

					});//ajax 끝		

		}//certificationData끝

		//사용툴 삭제
		$(document).on("click", "#toolDel", function() {

			$.ajax({
				url : "toolDelete.do",
				type : "post",
				dataType : "text",
				data : "toolNo=" + $(this).attr("name"),
				success : function(data) {
					alert("삭제되었습니다.");
					toolData();
				},
				error : function(data) {
					alert("삭제실패");
				}
			}); //ajax끝

		});
		toolData();

		$(document).on(
				"click",
				"#selfIntroBtn",
				function() {
					$.ajax({
						url : "selfIntroUpdate.do",
						type : "post",
						dataType : "text",
						data : "memberId=${memberId}&memberSelfIntro="
								+ $("#selfIntro").val(),
						success : function(data) {
							alert("등록되었습니다.");
						},
						error : function(data) {
							alert("등록실패");
						}
					}); //ajax끝
				});
		selfIntroData();

		function selfIntroData() {
			$.ajax({
				url : "selfIntroSelect.do",
				type : "post",
				dataType : "text",
				data : "memberId=${memberId}",
				contentType: "application/x-www-form-urlencoded; charset=UTF-8",
				success : function(data) {
					var textarea = data;

					$("#selfIntro").val(textarea);
				},
				error : function(data) {
					alert(data + "error 발생");
				}
			}); //ajax 끝
		};

	}); //완전 끝
</script>
</head>

<body>
	<div class="wrapper">
		<div class="header">
			<jsp:include page="../basicView/header.jsp" />
		</div>
		<div class="container content">
			<div class="row">
				<div class="col-md-3">
					<jsp:include page="../basicView/mypage_side.jsp"/>
				</div>
				<div class="col-md-9">
					<table id="title">
						<tr>
							<td colspan="5">
								<span class="fa fa-angle-right">&nbsp;&nbsp;경력 사항</span>
							</td>
						</tr>
					</table>
					<table id="careerTable" cellspacing="0">
						<tr>
							<th width="160px">회사 명</th>
							<th width="160px">재직 기간</th>
							<th width="100px">직급</th>
							<th width="100px">부서</th>
							<th width="40px"></th>
						</tr>
					</table>
					<form name="careerInsert.do" method="post" id="careerForm">
						<table id="inputTable" cellspacing="0">
							<tr>
								<td width="160px"><input type="text" name="careerCompany"
									id="careerCompany" placeholder="**전자"></td>
								<td width="160px"><input type="text" name="careerPeriod"
									id="careerPeriod" placeholder="2012.03 ~ 2014.02"></td>
								<td width="100px"><input type="text" name="careerPosition"
									id="careerPosition" placeholder="사원"></td>
								<td width="100px"><input type="text"
									name="careerDepartment" id="careerDepartment" placeholder="무선사업부"></td>
								<td width="40px"><button type="button" class="addBtn"
										value="추가" id="careerBtn">
										<span class="fa fa-plus"></span>
									</button></td>
							</tr>
						</table>
					</form>

					<br>
					<br>

					<table id="title">
						<tr>
							<td colspan="5"><span class="fa fa-angle-right">&nbsp;&nbsp;자격
									사항</span></td>
						</tr>
					</table>
					<table id="certificationTable" cellspacing="0">
						<tr>
							<th width="180px">자격사항 명</th>
							<th width="180px">발급 기관</th>
							<th width="180px">발급 일자</th>
							<th width="40px"></th>
						</tr>
						<tr></tr>
					</table>
					<form name="certificationInsert.do" method="post"
						id="certificationForm">
						<table id="inputTable" cellspacing="0">
							<tr>
								<td width="180px"><input type="text"
									name="certificationName" id="certificationName" placeholder="정보처리기사"></td>
								<td width="180px"><input type="text"
									name="certificationCompany" id="certificationCompany" placeholder="큐넷"></td>
								<td width="180px"><input type="text"
									name="certificationDate" id="certificationDate" placeholder="2013.10.03"></td>
								<td width="40px"><button type="button" class="addBtn"
										value="추가" id="certificationBtn">
										<span class="fa fa-plus"></span>
									</button></td>
							</tr>
						</table>
					</form>

					<br>
					<br>
                    <!-- 디자이너에게 보여지면 안되는 공간 시작 -->
                    <!-- div로 숨긴다. -->
                    <div id="programingTool">
					<table id="title">
						<tr>
							<td colspan="5"><span class="fa fa-angle-right">&nbsp;&nbsp;사용
									가능한 프로그래밍 언어</span></td>
						</tr>
					</table>
					<table id="languageTable" cellspacing="0">
						<tr>
							<th width="180px">언어 명</th>
							<th width="220px">활용 등급</th>
							<th width="120px">사용 기간</th>
							<th width="40px"></th>
						</tr>
					</table>
					<form name="languageInsert.do" method="post" id="languageForm">
						<table id="inputTable" cellspacing="0" align="center">
							<tr>
								<!-- 여기바꿔야됨 셀렉트박스로 -->
								<td width="180px"><select id="languageName"
									name="languageName">
										<option value="C">C</option>
										<option value="C++">C++</option>
										<option value="C#">C#</option>
										<option value="JAVA">JAVA</option>
										<option value="PHP">PHP</option>
										<option value="Ruby">Ruby</option>
										<option value="Python">Python</option>
										<option value="JSP">JSP</option>
										<option value="iOS">iOS</option>
										<option value="Tizen">Tizen</option>
										<option value="Spring">Spring</option>
										<option value="HybridApp">HybirdApp</option>
										<option value="Go">Go</option>
										<option value="Swift">Swift</option>
										<option value="Objective C">Objective C</option>
										<option value="전자정부프레임워크">전자정부프레임워크</option>										
								</select></td>
								<td width="220px"><input type="radio" size="12"
									name="languageLevel" value="1">★&nbsp;&nbsp; <input
									type="radio" size="12" name="languageLevel" value="2">★★&nbsp;&nbsp;
									<input type="radio" size="12" name="languageLevel" value="3">★★★&nbsp;&nbsp;
									<input type="radio" size="12" name="languageLevel" value="4">★★★★&nbsp;&nbsp;
									<input type="radio" size="12" name="languageLevel" value="5">★★★★★
								</td>
								<td width="120px"><input type="text" size="30"
									name="languagePeriod" id="languagePeriod"></td>
								<td width="40px"><button type="button" class="addBtn"
										value="추가" id="languageBtn">
										<span class="fa fa-plus"></span>
									</button></td>
							</tr>
						</table>
					</form>

					<br>
					<br>
					</div>
					<!-- 디자이너에게 보여질 공간 끝 -->
					<div id="designTool">
					<table id="title">
						<tr>
							<td colspan="5"><span class="fa fa-angle-right">&nbsp;&nbsp;사용
									가능한 디자인 툴</span></td>
						</tr>
					</table>
					<table id="toolTable" cellspacing="0">
						<tr>
							<th width="180px">툴 명</th>
							<th width="220px">활용 등급</th>
							<th width="120px">사용 기간</th>
							<th width="40px"></th>
						</tr>
					</table>
					<form name="toolInsert.do" method="post" id="toolForm">
						<table id="inputTable" cellspacing="0">
							<tr>
								<!-- 여기바꿔야됨 셀렉트박스로 -->
								<td width="180px"><select id="toolName" name="toolName">
										<option value="Photoshop">Photoshop</option>
										<option value="PowerMockup">PowerMockup</option>
										<option value="FrameBox">FrameBox</option>
								</select></td>
								<td width="220px"><input type="radio" size="12"
									name="toolLevel" value="1">★&nbsp;&nbsp; <input
									type="radio" size="12" name="toolLevel" value="2">★★&nbsp;&nbsp;
									<input type="radio" size="12" name="toolLevel" value="3">★★★&nbsp;&nbsp;
									<input type="radio" size="12" name="toolLevel" value="4">★★★★&nbsp;&nbsp;
									<input type="radio" size="12" name="toolLevel" value="5">★★★★★&nbsp;&nbsp;
								</td>
								<td width="120px"><input type="text" size="30"
									name="toolPeriod" id="toolPeriod"></td>
								<td width="40px">
									<button type="button" class="addBtn" value="추가" id="toolBtn">
										<span class="fa fa-plus"></span>
									</button>
								</td>
							</tr>
						</table>
					</form>
						</div>
					<br>
					<br>

					<table id="title">
						<tr>
							<td colspan="5"><span class="fa fa-angle-right">&nbsp;&nbsp;자기
									소개</span></td>
						</tr>
					</table>

					<!-- th 들어갈 부분 -->

					<form name="selfIntroUpdate.do" method="post" id="selfIntroForm">
						<table id="inputTable" cellspacing="0">
							<tr>
								<td width="520px"><textarea rows="3" name="selfIntro"
										id="selfIntro" placeholder="프리랜서 고르기 메뉴 내 목록에서 보여질 항목입니다."></textarea>
								</td>
								<td width="40px">
									<button type="button" class="addBtn" value="입력"
										id="selfIntroBtn">
										<span class="fa fa-check"></span>
									</button>
								</td>
							</tr>
						</table>
					</form>
					<br>
					<br>
					<br>
					<br>
					<br>
				</div>
			</div>
		</div>
		<div class="footer">
			<jsp:include page="../basicView/footer.jsp" />
		</div>
	</div>
</body>