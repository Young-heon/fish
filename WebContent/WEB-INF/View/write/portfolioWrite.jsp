<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="ko">
<head>
<style>
</style>

<jsp:include page="../basicView/header.jsp" />

<!-- datepicker -->

<script src="../js/daterangepicker.min.js"></script>

<link href="../css/bootstrap.css" rel="stylesheet">
<link href="http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" media="all" href="../css/daterangepicker-bs3.css" />
<script type="text/javascript" src="../js/jquery-1.11.1.min.js"></script>
<script type="text/javascript"src="http://netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
<script type="text/javascript" src="../js/moment.js"></script>
<script type="text/javascript" src="../js/daterangepicker.js"></script>


<!--Menu End-->
<script type="text/javascript">
	$(function() {
		
		var memberCheckCode=${memberCheckCode};
		if(memberCheckCode==1){ // 제공자는1			
			
		}else{
			$("#point").hide();
		}
		
		$(".resume-li").addClass('active');
	});
</script>

<script>
	$(function() {
		
		function readURL(input) {
			if (input.files && input.files[0]) {
				var reader = new FileReader();
				reader.onload = function(e) {
					$("#portfolio_image").attr('src', e.target.result);
				}
				reader.readAsDataURL(input.files[0]);
			}
		}
		
		if ($("#portfolio_image").attr('src') == '') {
			$("#portfolio_image").attr('src', "../image/jonathan.png");
		}

		$("#imageUpload").bind("click", function() {
			$("#imageUploadForm").submit();
		});

		//datePicker
		$('#portfolioPeriod').daterangepicker(
						null,
						function(start, end, label) {
							console.log(start.toISOString(), end.toISOString());
							$("#portfolioStartDate").attr("value",
									start.toISOString());
							$("#portfolioEndDate").attr("value",
									end.toISOString());
						});

		function portfolioData() {

			$.ajax({
						url : "portfolioSelect.do",//서버에 요청할 정보
						type : "post",
						dataType : "json",//결과 데이터 타입
						success : function(data) {//성공했을때
							var table = "";

							//기존에 있는 테이블 첫행만 빼고 지우기
							$("#portfolioTable tr:gt(0)").remove();

							$(data.list)
									.each(
											function(index, item) {
												table += "<tr>";
												table += "<th>번호</th>";
												table += "<td>" + item.portfolioNo + "</td>";
												table += "<td width='10%'><button type='button' class='addBtn' value='삭제' id='portfolioDel' name='"+item.portfolioNo+ "'><span class='fa fa-trash-o'></span></button></td>";
												table += "</tr>";
												table += "<tr>";
												table += "<th>프로젝트 제목</th>";
												table += "<td colspan='2'>" + item.portfolioName + "</td>";
												table += "</tr>";
												table += "<tr>";
												table += "<th>시작일</th>";
												table += "<td colspan='2'>" + item.portfolioStartDate.substr(0,10) + "</td>";
												table += "</tr>";
												table += "<tr>";
												table += "<th>종료일</th>";
												table += "<td colspan='2'>" + item.portfolioEndDate.substr(0,10) + "</td>";
												table += "</tr>";
												table += "<tr>";
												table += "<th>역할</th>";
												table += "<td colspan='2'>" + item.portfolioPosition + "</td>";
												table += "</tr>";
												table += "<tr>";
												table += "<th>세부 사항</th>";
												table += "<td colspan='2'>" + item.portfolioDetail + "</td>";
												table += "</tr>";
												table += "<tr>";
												table += "<th>이미지</th>";
												//table += "<td colspan='2'>"+item.portfolioImage+"</td>";
												table+="<td colspan='2'>";
												table+="<img id='portfolio_image' class='media-object' src='http://localhost:9090/${portfolioImgUrl}' width='100px'";
												table+="height='100px' alt=''>";
												
												table += "</tr>";
											
												
												
												
												table += "<tr><td><br><br></td></tr>";
																		
											});//반복문끝

							//테이블에 추가한다.
							$("#portfolioTable tr:eq(0)").after(table);
						},
						error : function(data) {
							//alert(data+"error발생");
						}

					});//ajax 끝		
		}

		//사용툴 삭제
		$(document).on("click", "#portfolioDel", function() {

			$.ajax({
				url : "portfolioDelete.do",
				type : "post",
				dataType : "text",
				data : "portfolioNo=" + $(this).attr("name"),
				success : function(data) {
					alert("삭제되었습니다.");
					portfolioData();
				},
				error : function(data) {
					alert("삭제실패");
				}
			}); //ajax끝

		})
		portfolioData();

	}) //완전 끝
</script>


</head>

<body>
	<div class="wrapper">
		<div class="header">
		</div>
		<div class="container content">
			<div class="row">
				<div class="col-md-3">
					<jsp:include page="../basicView/mypage_side.jsp"/>
				</div>
				<div class="col-md-9">
					<table id="portfolioTitle">
						<tr>
							<td><span class="fa fa-angle-right">&nbsp;&nbsp;포트폴리오에
									프로젝트 등록하기</span></td>
						</tr>
					</table>
					<table id="portfolioInputTable">
						<tr>
						<td>
						<br>
						<form id="portfolioForm" action="portfolioInsert.do" method="post" class="clearfix">
							<div class="login-bar hidden-xs"></div>
							<div class="login-right">
								<input type="hidden" name="memberId" id="memberId"
									value="${memberId}">
								<div class="form-group">
									<label for="projectAmount">프로젝트 명 *</label> <input type="text"
										name="portfolioName" id="portfolioName" class="form-control"
										placeholder="홈페이지 제작" required />
								</div>
								<label for="projectDate">프로젝트 기간 *</label> <input type="hidden"
									id="portfolioStartDate" name="portfolioStartDate" value="" /> 
								<input type="hidden" id="portfolioEndDate" name="portfolioEndDate" value="" required />
								<div class="projectDate" style="margin-bottom: 15px;">
									<div class="control-group">
										<div class="controls">
											<div class="input-prepend input-group">
												<span class="add-on input-group-addon"><i
													class="glyphicon glyphicon-calendar fa fa-calendar"></i></span><input
													type="text" style="width: 200px" name="portfolioPeriod"
													id="portfolioPeriod" class="form-control"
													value="2014/08/01 - 2014/08/22" required />
											</div>
										</div>
									</div>
								</div>
								<div class="form-group">
								<label for="projectTech">역할 *</label> 
								<input type="text" size="30" name="portfolioPosition" id="portfolioPosition"
									class="form-control" placeholder="해당 프로젝트에서 맡았던 역할을 기술하세요"
									required />
								</div>
	
								<div class="form-group">
									<label for="projectTech">세부사항 *</label>
									<textarea name="portfolioDetail" id="portfolioDetail" class="form-control" rows="5" placeholder="프로젝트에 대해서 자세히 설명하시오" required>
									</textarea>
								</div>	
											
	
								<div align="right">
									<button type="submit" class="butn butn-warning butn-block"
										id="portfolioBtn">포트폴리오 등록하기</button>
								</div>
							</div>
						</form>	
							
						<div class="form-group">
									<form id="imageUploadForm" method="post" action="uploadPortfolio.do"
										enctype="multipart/form-data">	<input type="file" name="file" id="file"
											onchange="readURL(this);" />
											<input type="hidden" id="memberId" name="${memberId}" />
										<label for="projectTech">이미지</label>
										<button id="imageUpload">이미지 등록</button>
									</form>
							</div>	
						
						</td>
						</tr>
					</table>
					<br><br>

					<table id="portfolioTitle">
						<tr>
							<td>
								<span class="fa fa-angle-right">&nbsp;&nbsp;내 포트폴리오 목록</span>
							</td>
						</tr>
					</table>
					<table id="portfolioTable" cellspacing="0">
						<tr>
							<td><br></td>
						</tr>
					</table>					
					<br> <br> <br> <br> <br>
				</div>
			</div>
		</div>
		<div class="footer">
			<jsp:include page="../basicView/footer.jsp" />
		</div>
	</div>
</body>
</html>