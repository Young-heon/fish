<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!-- header부분 -->
<head>
<!-- =================
	meta tag
================= -->
<meta charset="utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />

<!-- =================
	title
================= -->
<title>Let's go Dogether!</title>

<!-- =================
	CSS
================= -->
<link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap-glyphicons.css" rel="stylesheet">
<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
<link href="/Fish/css/bootstrap.css" rel="stylesheet" type="text/css" charset="utf-8">
<link href="/Fish/css/animate.css" rel="stylesheet" type="text/css">
<link href="/Fish/css/custom.css" rel="stylesheet" type="text/css">

<!-- =================
	JavaScript
================= -->
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="http://www.azurekyle.com/rnm/redesign/js/bootstrap.js"></script>
<script src="/Fish/js/jquery-ex.js"></script>


<script>
$(document).ready(function(){
	$("#addInfo").modal('show'); 
});
</script>
</head>

<body>
	<div class="navbar navbar-default navbar-fixed-top" role="navigation">
		<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse"
					data-target=".navbar-collapse">
					<span class="sr-only">Toggle navigation</span> <span
						class="icon-bar"></span> <span class="icon-bar"></span> <span
						class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="#">Dogether</a>
			</div>
			<div class="navbar-collapse collapse">
				<ul class="nav navbar-nav">
					<li class="active">
						<a href="#">Dogether?</a>
					</li>
					<li class="active">
						<a href="#" data-target="#modal_Login" data-toggle="modal" class="topMenu">프로젝트 고르기</a>
					</li>
					<li class="active">
						<a href="#" data-toggle="modal" data-target="#modal_Login" class="topMenu">프리랜서 고르기</a>
					</li>
				</ul>
				<ul class="nav navbar-nav navbar-right">
					<li><a href="#" data-target="#modal_Login" data-toggle="modal">로그인</a></li>
					<li><a href="#" data-target="#modal_Join" data-toggle="modal">회원가입</a></li>
				</ul>
			</div>
		</div>
	</div>
	<!--Menu End-->
	<!-- Dogether Section -->
	<section class="dogether" id="dogether"
		style="padding-top: 0px; padding-bottom: 50px;">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<img class="img-responsive" src="../image/main.png" alt="" style=" margin-left: 70px;">
				</div>
				<div class="col-md-12 text-center">
					<h2>Dogether</h2>
				</div>
			</div>
			<div class="row">
				<hr class="star-light">
				<span class="skills">Between Developer &amp; Designer</span>
			</div>
		</div>
	</section>
	<br>
	<br>
	<br>
	<br>
	<!-- Developer Section -->
	<section class="dogether2" id="dogether2" style="height: 550px;">
		<div class="container col-md-12" align="center">
			<iframe frameborder="0" width="800" height="100%" src="../tagsphere.html" scrolling="no"></iframe>
		</div>
	</section>

	<br>
	<br>
	<br>
	<br>
	<br>
	<div class="container" style="height: 500px;">
		<!--content-wrapper-->
		<div class="modal" id="addInfo">
			<div class="modal-join-dialog contcustom modal-join-dialog-center">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal"
							aria-hidden="true">&times;</button>

						&nbsp;&nbsp;&nbsp;<span class="glyphicon glyphicon-user bigicon"></span>
						<h2 id="join-font">Add Info</h2>
						<h4>소셜 로그인 이용자는 추가 정보를 필요로 합니다.</h4>
					</div>
					<div class="modal-body">
						<form id="registerMember" action="memberInsert.do" method="post" role="form" class="clearfix">
							<fieldset id="acField">

								<div class="login-bar hidden-xs"></div>
								<div class="login-right"></div>
								
								<div class="form-group">
										<input name="memberPhone" id="memberPhone" type="text" class="form-control" placeholder="your phone number" />
								</div>
									<p>
									<input type="radio" name="memberCheckCode" id="memberCheckCode" value="1"> 
									<span class="memberCheck">프로젝트를 제공하려고 합니다.<br /></span>
									<span class="memberCheck">프로젝트를 찾고 있는&nbsp;</span>
									<input type="radio" name="memberCheckCode" id="memberCheckCode" value="2">
									<span class="memberCheck">개발자입니다.</span>
									<input type="radio" name="memberCheckCode" id="memberCheckCode" value="3">
									<span class="memberCheck">디자이너입니다.<br /></span>
									</p>
									<br>
									<button type="submit" class="butn butn-warning wide butn-block" id="facebookAdd">
										<span class="glyphicon glyphicon-ok med"></span>
									</button>	
							</fieldset>
						</form>
					</div>
				</div><!-- /.modal-content -->
			</div><!-- /.modal-dialog -->
		</div><!-- /.modal -->
	</div><!-- Content End -->
</body>

<jsp:include page="../basicView/footer.jsp" />

</html>