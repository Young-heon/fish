<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="ko">
<jsp:include page="basicView/header.jsp" />

<script>
	$(function() {
		
		var memberCheckCode=${memberCheckCode};
		if(memberCheckCode==1){ // 제공자는1			
			
		}else{
			$("#point-li").hide();
		};
	});
</script>

<body>
	<div class="wrapper">
		<div class="header">
		</div>
		<div class="container content">
			<div class="row">
				<div class="col-md-3">
					<jsp:include page="basicView/mypage_side.jsp"/>
				</div>
				<div class="col-md-9">
					준비중인 메뉴입니다.
				</div>
			</div>
		</div>
		<div class="footer">
			<jsp:include page="basicView/footer.jsp" />
		</div>
	</div>
</body>
</html>