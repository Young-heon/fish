<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="ko">


<head>
<jsp:include page="basicView/header.jsp" />

<script>
$(document).ready(function(){
	var click_count=1;
	//Load More 버튼 클릭 하시 동작한다.
		$.ajax({
			url:"projectPaging.do",
			type:"post",
			dataType:"json",
			data:"projectNo="+click_count,
			success:function(getData){
				var result="";		
				getData.projectPaging.sort(function(a,b){
					var x = a.projectNo > b.projectNo ? -1: 1;
					return x;
				});				
				$(getData.projectPaging).each(function(index,item){
					var year = item.projectStartDate.substring(0,4);
					var year2 =  item.projectEndDate.substring(0,4);
					var month =  item.projectStartDate.substring(5,7);
					var month2 =  item.projectEndDate.substring(5,7);
					var day =  item.projectStartDate.substring(8,10);
					var day2 = item.projectEndDate.substring(8,10);
					var date1 = new Date(year,month,day);
					var date2 = new Date(year2,month2,day2);
					var gap = (date2-date1)/(1000*60*60*24);
					
					result+='<div class="col-sm-6 scrollpoint sp-effect active"><div class="media">';
					result+='<div class="media-body" id="'+item.projectNo+'"><h4 class="media-heading">';
					result+=item.projectSubject;
					result+='</h4><h6><p><span class="fa fa-won">';
					result+='  예상금액 ';
					result+=item.projectAmount;//예상 금액
					result+=' 원&nbsp;</span><span class="glyphicon glyphicon-time"> 예상기간 ';
					result+=gap;//예상기간
					result+=' 일&nbsp;</span><span class="glyphicon glyphicon-map-marker"> 진행지역 ';
					result+=item.projectLocation; //진행지역
					result+='</span><span class="glyphicon glyphicon-calendar"> 등록일자 '
					result+=item.projectEnrollStart.substr(0,10);
					result+='&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span><span class="glyphicon glyphicon-thumbs-up"> 총 ';
					result+=item.projectPeople;
					result+='명 지원</span>';
					result+='</p><p>';
					result+=item.projectDetail.substr(0,100)+'...'; //일단은 100자만 보여준다
					result+='</p><p> 요구 기술 : ';
					result+=item.projectTech;
					result+='</p></h6></div></div></div>';
					
				});
				$("#printProject").html(result);
			},
			error:function(err){
				alert(err+"에러 발생");
			}
		}); //ajax end
			
		$(document).on("click",".media-body",function(){
			location.href="project.do";
		});		
	
});
</script>
</head>

<body>
	<!-- Dogether Section -->
	<section class="dogether" id="dogether"
		style="padding-top: 0px; padding-bottom: 50px;">
		<div class="container">
			<div class="row">
				<div>
					<div class="col-md-12">
						<img class="img-responsive" src="./image/main_animate.gif" alt="" style=" margin-left: 70px;">
					</div>

					<div class="col-lg-12 text-center">
						<h2>Dogether</h2>

					</div>
				</div>
			</div>
			<div class="row">
				<hr class="star-light">
				<span class="skills">Between Developer & Designer</span>
			</div>
		</div>
	</section>


	<br>
	<br>
	<br>
	<br>
	
	<section class="dogether2" id="dogether2" style="height: 550px;">
		<div class="container col-md-12" align="center" style="height: 530px;">
			<iframe frameborder="0" width="800" height="100%" src="tagsphere.html" scrolling="no" style="height: inherit;" >
			</iframe>
		</div>
	</section>

	<br>
	<br>
	<br>
	<br>
	<br>

	<!-- Project Grid Section -->
	<section id="portfolio">
		<div class="container">
			<div class="row">
				<div class="col-lg-12 text-center">
					<h2>Project List</h2>
					<hr class="star-primary">
				</div>
			</div>
		</div>
	</section>

	<section id="card">
		<div class="row" id="printProject">
		</div>
	</section>

	<br>
	<br>
	<br>
	<br>
	<br>
	<br>
	<br>
	<br>

	<!-- modal 프로젝트 상세페이지 -->
	<div class="modal" id="modal_ProjectDetail"></div>
	<!-- Content End -->
</body>

<jsp:include page="basicView/footer.jsp" />

</html>