<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="ko">

<script type="text/javascript">
</script>

<jsp:include page="basicView/header.jsp" />

<body>
	<!--Menu End-->
	<div class="container" align="center">
		<img src="image/intro.png"/>
		<br><br><br><br>
		<p>
			<table class="introTable">
				<tr>
					<th>
						<h3>Developer + Designer , Together = Dogether</h3>
					</th>
				</tr>
				<tr>
					<td>
						<br><br><br>
						문제 1. 현재 디자이너와 개발자는 자신에게 맞는 프로젝트를 찾기가 어렵습니다. <br>
						문제 2. 프로젝트 제공자는 필요한 조건에 맞는 디자이너나 개발자를 구하기가 어렵습니다. <br> <br>
						
						이러한 문제점을 해결하기 위해서 고심 끝에 탄생한 'DOGETHER'는 프로젝트 제공자와 <br>
						 디자이너 또는 개발자 사이를 연결시켜줄 수 있는 사이트입니다. <br> <br>
						
						많은 사람들이 'DOGETHER' 안에서 하고싶은 프로젝트를 함께 할 수 있었으면 좋겠습니다. <br><br><br><br>
					</td>
				</tr>
				<tr>
					<td>
						우 리 사 진 들 어 갈 곳
					</td>
					
				</tr>
			</table>
		<br><br><br><br>
		<br><br><br><br>
		</p>
	</div>

	<jsp:include page="basicView/footer.jsp" />
</body>
</html>