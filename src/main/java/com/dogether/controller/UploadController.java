package com.dogether.controller;

import java.io.File;
import java.util.HashMap;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.dogether.dao.MemberDao;
import com.dogether.service.MemberService;
import com.dogether.vo.PortfolioImageVo;
import com.dogether.vo.Upload2Vo;
import com.dogether.vo.UploadVo;

@Controller
public class UploadController {
	
	@Autowired
	MemberService memberService;
	MemberDao memberDao;
	
	@RequestMapping("modify/upload2.do")
	public ModelAndView bb(@ModelAttribute("vo")UploadVo vo,HttpSession session){
		//파일 이름
		String fName = vo.getFile().getOriginalFilename();
		System.out.println("fName : "+fName);
		String fPath = session.getServletContext().getRealPath("/userImage/");
		System.out.println(fPath);
		try{
			vo.getFile().transferTo(new File(fPath+"/"+fName));
			
		}catch(Exception e){
			
		}
		//path + fName
		String memberId = (String)session.getAttribute("memberId");
		String path = session.getServletContext().getContextPath() + "/userImage";
		
		HashMap<String,String> param = new HashMap<String,String>();
		param.put("memberId",memberId);
		param.put("filePath",path+"/"+fName);
		memberService.memberImage(param);
		
		String memberImgUrl = memberService.memberGetImage(memberId);
		System.out.println(memberImgUrl);
		session.setAttribute("memberImgUrl", memberImgUrl);
		ModelAndView mv =new ModelAndView();
		mv.setViewName("mypage"); //WEB-inf/view/upload.result.jsp로 이동
		return mv;
		
	}
	@RequestMapping("write/uploadPortfolio.do")
	public ModelAndView portfolioImage(@ModelAttribute("vo")Upload2Vo vo,HttpSession session){
		
		String fName = vo.getFile().getOriginalFilename();
		System.out.println("fName : "+fName);
		String fPath = session.getServletContext().getRealPath("/portfolioImage/");
		String portfolioNo=vo.getPortfolioNo();
		System.out.println(fPath);
		System.out.println(portfolioNo);
		try{
			vo.getFile().transferTo(new File(fPath+"/"+fName)); 
			
		}catch(Exception e){
			
		}
		//path + fName
		String memberId = (String)session.getAttribute("memberId"); 
		String path = session.getServletContext().getContextPath() + "/portfolioImage";
		
		HashMap<String,String> param2=new HashMap<String,String>();
		param2.put("memberId",memberId);
		param2.put("portfolioNo",portfolioNo);
		param2.put("filePath",path+"/"+fName);//왼쪽 변수는 xml과 이름 같아야!
		memberService.portfolioImage(param2);//맵을 넘겨줌
		
		HashMap<String,String> param3=new HashMap<String,String>();
		param2.put("memberId",memberId);
		param2.put("portfolioNo",portfolioNo);
		String portfolioImgUrl=memberService.portfolioImageGet(param3);//멤버아이디랑 포트폴리오번호
		
		System.out.println(portfolioImgUrl);
		session.setAttribute("portfolioImgUrl", portfolioImgUrl); //주소값 세션에 저장
		ModelAndView mv =new ModelAndView();
		mv.setViewName("mypage"); //WEB-inf/view/upload.result.jsp로이동??
		return mv;
		
	}
}
