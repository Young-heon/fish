package com.dogether.service;

import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.dogether.dao.MemberDao;
import com.dogether.vo.CareerVo;
import com.dogether.vo.CertificationVo;
import com.dogether.vo.LanguageVo;
import com.dogether.vo.MemberEnrollProjectVo;
import com.dogether.vo.MemberVo;
import com.dogether.vo.PortfolioVo;
import com.dogether.vo.ProjectVo;
import com.dogether.vo.ToolVo;

public class MemberServiceImpl implements MemberService{
	

	@Autowired
	private MemberDao memberDao;

	
	//회원가입 DAO호출
	@Override
	public int memberInsert(MemberVo vo) {
		
		return memberDao.memberInsert(vo);
	}

	// ID 중복 확인 DAO호출
	@Override
	public MemberVo memberIdCheck(String memberId) {
		
		return memberDao.memberIdCheck(memberId);
	}


	//가입한 회원인지 체크하는 DAO호출
	@Override
	public MemberVo memberLogin(HashMap<String,String> loginInfo) {
		
		return memberDao.memberLogin(loginInfo);
		
		
	}

	//프로젝트 등록하는 DAO 호출
	@Override
	public int projectInsert(ProjectVo vo) {
		
		return memberDao.projectInsert(vo);
	}

	@Override
	public int memberImage(HashMap<String, String> imageUrl) {
		
		return memberDao.memberImage(imageUrl);
	}

	@Override
	public String memberGetImage(String memberId) {
		
		return memberDao.memberImageGet(memberId);
	}

	//포트폴리오 이미지 부분
	@Override
	public int portfolioImage(HashMap<String, String> imageUrl) {
		// TODO Auto-generated method stub
		return memberDao.portfolioImage(imageUrl);
	}

	@Override
	public String portfolioImageGet(HashMap<String, String> imageUrl) {
		// TODO Auto-generated method stub
		return memberDao.portfolioImageGet(imageUrl);
	}
	
	
	
	
	@Override
	public ProjectVo projectDetail(int projectNo) {
		return memberDao.projectDetail(projectNo);
	}

	
	//프로젝트 페이징
	@Override
	public List<ProjectVo> projectPaging(int projectNo) {
		
		return memberDao.projectPaging(projectNo);
	}
	
	//프리랜서 페이징
	@Override
	public List<MemberVo> freePaging(HashMap<String,String> memberInfo) {
		
		return memberDao.memberPaging(memberInfo);
	}


	
	//페이스북 로그인 기능
	@Override
	public int socialLogin(HashMap<String, String> socialInfo) {
		
		return memberDao.socialLogin(socialInfo);
	}
	
	//페이스북 소셜 테이블에 값 있는지 검사 여부와 동시에, 체크한다
	@Override
	public int socialCheck(HashMap<String, String> socialId) {
		
		return memberDao.socialCheck(socialId);
	}
	
	//이력서-경력추가
	@Override
	public int careerInsert(CareerVo vo){
		return memberDao.careerInsert(vo);
	}

	@Override
	public List<CareerVo> careerSelect(String memberId) {
			
		return memberDao.careerSelect(memberId);
	}
		
	@Override
	public int careerDelete(int careerNo){
		return memberDao.careerDelete(careerNo);
	}

	

	//자격증 부분
	@Override
	public int certificationInsert(CertificationVo vo) {
		// TODO Auto-generated method stub
		return memberDao.certificationInsert(vo);
	}

	@Override
	public List<CertificationVo> certificationSelect(String memberId) {
		// TODO Auto-generated method stub
		return memberDao.certificationSelect(memberId);
	}

	@Override
	public int certificationDelete(int certificationNo) {
		// TODO Auto-generated method stub
		return memberDao.certificationDelete(certificationNo);
	}


	//언어부분
	@Override
	public int languageInsert(LanguageVo vo) {
		
		return memberDao.languageInsert(vo);
	}

	@Override

	public List<LanguageVo> languageSelect(String memberId) {
		// TODO Auto-generated method stub
		return memberDao.languageSelect(memberId);
	}

	@Override
	public int languageDelete(int languageNo) {
		// TODO Auto-generated method stub
		return memberDao.languageDelete(languageNo);

	}
	
	//툴
	@Override
	public int toolInsert(ToolVo vo) {
		// TODO Auto-generated method stub
		return memberDao.toolInsert(vo);
	}

	@Override
	public List<ToolVo> toolSelect(String memberId) {
		// TODO Auto-generated method stub
		return memberDao.toolSelect(memberId);
	}

	@Override
	public int toolDelete(int toolNo) {
		// TODO Auto-generated method stub
		return memberDao.toolDelete(toolNo);

	}
	
	//이력서 자기소개
	public int selfIntroUpdate(String memberId, String memberSelfIntro){
		// TODO Auto-generated method stub
		return memberDao.selfIntroUpdate(memberId, memberSelfIntro);
	}
	
	public String selfIntroSelect(String memberId){
		return memberDao.selfIntroSelect(memberId);
	}

	//포트폴리오 
	@Override
	public int portfolioInsert(PortfolioVo vo) {
		// TODO Auto-generated method stub
		return memberDao.portfolioInsert(vo);
	}

	@Override
	public List<PortfolioVo> portfolioSelect() {
		// TODO Auto-generated method stub
		return memberDao.portfolioSelect();
	}

	@Override
	public int portfolioDelete(int portfolioNo) {
		// TODO Auto-generated method stub
		return memberDao.portfolioDelete(portfolioNo);

	}

	
	//멤버 이름 가져온다.
	@Override
	public String memberGetName(String memberId) {
		
		return memberDao.memberGetName(memberId);
	}

	
	//유저가 프로젝트에 참가신청하는 메소드
	@Override
	public int memberEnrollProject(HashMap<String, String> enrollMap) {
		
		return memberDao.memberEnrollProject(enrollMap);
	}
	
	//유저가 등록한 매칭전인 프로젝트 정보를 가져온다.
	@Override
	public List<ProjectVo> userBeforeProject(String EnrollUser) {
		
		return memberDao.userBeforeProject(EnrollUser);

	}

	//유저가 진행중인 프로젝트 정보를 가져온다.
	@Override
	public List<ProjectVo> userOngoingProject(String enrollUser) {
			
		return memberDao.userOngoingProject(enrollUser);
	}

	@Override
	public List<ProjectVo> userDoneProject(String enrollUser) {
			
		return memberDao.userDoneProject(enrollUser);
	}

	@Override
	public String memberEnrollProjectCheck(HashMap<String, String> enrollCheck) {
		
		return memberDao.memberEnrollProjectCheck(enrollCheck);
	}
	
	//멤버의 비밀번호 변경
	@Override
	public int memberPasswordChange(HashMap<String, String> changePass) {
		
		return memberDao.memberPasswordChange(changePass);

	}
	
	//멤버의 체크코드를 가져온다.
	@Override
	public String memberGetCheckCode(String memberId) {
		
		return memberDao.memberGetCheckCode(memberId);
	}

	//유저의 핸드폰번호변경
	@Override
	public int memberPhoneChange(HashMap<String, String> changePhone) {
		// TODO Auto-generated method stub
		
		return memberDao.memberPhoneChange(changePhone);
	}
	//주소변경 
	@Override
	public int memberAddrChange(HashMap<String, String> changeAddr) {
		// TODO Auto-generated method stub
		return memberDao.memberAddrChange(changeAddr);
	}

	
	//프로젝트 제공자의 매칭전인 프로젝트 정보를 가져온다.
	
	@Override
	public List<ProjectVo> supplyBeforeProjectList(String memberId) {
	
		return memberDao.supplyBeforeProjectList(memberId);
	}

	//프로젝트에 등록된 회원수를 가져온다.
	@Override
	public String enrollProjectCount(String ProjectNo) {
		
		return memberDao.enrollProjectCount(ProjectNo);
	}

	@Override
	public int updateProjectPeople(HashMap<String, String> updatePeople) {
	
		return memberDao.updateProjectPeople(updatePeople);
	}
	
	@Override
	public String getAthenCode(String memberId) {
		
		return memberDao.getAthenCode(memberId);
	}

	@Override
	public String projectGetImage(String projectNo) {
		
		return memberDao.projectGetImage(projectNo);
	}


	@Override
	public List<MemberEnrollProjectVo> getProjectEnrollList(String memberId) {
		
		return memberDao.getProjectEnrollList(memberId);
	}


		//
	@Override
	public String getDeveloperImage(String memberId) {
		// TODO Auto-generated method stub
		return memberDao.getDeveloperImage(memberId);
	}

	@Override
	public List<MemberVo> tagSearching(String searchTag) {
		// TODO Auto-generated method stub
		return memberDao.tagSearching(searchTag);
	}


	
	
}



