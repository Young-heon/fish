package com.dogether.vo;

public class MemberVo {
	private String memberId;
	private String memberPass;
	private int memberAthenCode;
	private String memberName;
	private String memberEmail;
	private int memberPoint;
	private String memberPhone;
	private int memberNo;
	private String memberImage;
	private int memberCheckCode;
	private String memberSelfIntro;
	
	
	public int getMemberCheckCode() {
		return memberCheckCode;
	}
	public void setMemberCheckCode(int memberCheckCode) {
		this.memberCheckCode = memberCheckCode;
	}
	public String getMemberPass() {
		return memberPass;
	}
	public void setMemberPass(String memberPass) {
		this.memberPass = memberPass;
	}
	public String getMemberId() {
		return memberId;
	}
	public void setMemberId(String memberId) {
		this.memberId = memberId;
	}
	
	public int getMemberAthenCode() {
		return memberAthenCode;
	}
	public void setMemberAthenCode(int memberAthenCode) {
		this.memberAthenCode = memberAthenCode;
	}
	public String getMemberName() {
		return memberName;
	}
	public void setMemberName(String memberName) {
		this.memberName = memberName;
	}
	public String getMemberEmail() {
		return memberEmail;
	}
	public void setMemberEmail(String memberEmail) {
		this.memberEmail = memberEmail;
	}
	public int getMemberPoint() {
		return memberPoint;
	}
	public void setMemberPoint(int memberPoint) {
		this.memberPoint = memberPoint;
	}
	public String getMemberPhone() {
		return memberPhone;
	}
	public void setMemberPhone(String memberPhone) {
		this.memberPhone = memberPhone;
	}
	public int getMemberNo() {
		return memberNo;
	}
	public void setMemberNo(int memberNo) {
		this.memberNo = memberNo;
	}
	public String getMemberImage() {
		return memberImage;
	}
	public void setMemberImage(String memberImage) {
		this.memberImage = memberImage;
	}
	public String getMemberSelfIntro() {
		return memberSelfIntro;
	}
	public void setMemberSelfIntro(String memberSelfIntro) {
		this.memberSelfIntro = memberSelfIntro;
	}
	
}
