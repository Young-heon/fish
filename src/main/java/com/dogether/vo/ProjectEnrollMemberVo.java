package com.dogether.vo;

public class ProjectEnrollMemberVo {
	private String memberId;
	private String enrollSupplier;
	private int projectStatus;
	
	public String getMemberId() {
		return memberId;
	}
	public void setMemberId(String memberId) {
		this.memberId = memberId;
	}
	public String getEnrollSupplier() {
		return enrollSupplier;
	}
	public void setEnrollSupplier(String enrollSupplier) {
		this.enrollSupplier = enrollSupplier;
	}
	public int getProjectStatus() {
		return projectStatus;
	}
	public void setProjectStatus(int projectStatus) {
		this.projectStatus = projectStatus;
	}
	
	
}
