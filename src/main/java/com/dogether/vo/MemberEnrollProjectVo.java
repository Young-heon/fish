package com.dogether.vo;

public class MemberEnrollProjectVo {
	private int projectNo;
	private String enrollUser;
	private int projectSupplyStatus;
	
	public int getProjectNo() {
		return projectNo;
	}
	public void setProjectNo(int projectNo) {
		this.projectNo = projectNo;
	}
	public String getEnrollUser() {
		return enrollUser;
	}
	public void setEnrollUser(String enrollUser) {
		this.enrollUser = enrollUser;
	}
	public int getProjectSupplyStatus() {
		return projectSupplyStatus;
	}
	public void setProjectSupplyStatus(int projectSupplyStatus) {
		this.projectSupplyStatus = projectSupplyStatus;
	}
	
	
	

}
