package com.dogether.vo;

import org.springframework.web.multipart.MultipartFile;

public class UploadVo {
	
	private String memberId;
	private String name;
	private MultipartFile file;
	
	public String getMemberId() {
		return memberId;
	}
	public void setMemberId(String memberId) {
		this.memberId = memberId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public MultipartFile getFile() {
		return file;
	}
	public void setFile(MultipartFile file) {
		this.file = file;
	}
}
