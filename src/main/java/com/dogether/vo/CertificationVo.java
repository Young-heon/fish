 package com.dogether.vo;

public class CertificationVo {
	private String memberId;
	private int certificationNo;
	private String certificationName;
	private String certificationCompany;
	private String certificationDate;
	
	public String getMemberId() {
		return memberId;
	}
	public void setMemberId(String memberId) {
		this.memberId = memberId;
	}
	public int getCertificationNo() {
		return certificationNo;
	}
	public void setCertificationNo(int certificationNo) {
		this.certificationNo = certificationNo;
	}
	public String getCertificationName() {
		return certificationName;
	}
	public void setCertificationName(String certificationName) {
		this.certificationName = certificationName;
	}
	public String getCertificationCompany() {
		return certificationCompany;
	}
	public void setCertificationCompany(String certificationCompany) {
		this.certificationCompany = certificationCompany;
	}
	public String getCertificationDate() {
		return certificationDate;
	}
	public void setCertificationDate(String certificationDate) {
		this.certificationDate = certificationDate;
	}
	
	
	
}
