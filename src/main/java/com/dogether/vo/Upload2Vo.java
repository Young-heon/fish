package com.dogether.vo;

import org.springframework.web.multipart.MultipartFile;

public class Upload2Vo {

	private String memberId;
	private String portfolioNo;
	private MultipartFile file;
	
	public String getMemberId() {
		return memberId;
	}
	public void setMemberId(String memberId) {
		this.memberId = memberId;
	}
	public String getPortfolioNo() {
		return portfolioNo;
	}
	public void setPortfolioNo(String portfolioNo) {
		this.portfolioNo = portfolioNo;
	}
	public MultipartFile getFile() {
		return file;
	}
	public void setFile(MultipartFile file) {
		this.file = file;
	}
	

}
