package com.dogether.vo;

public class ToolVo {
	//사용툴
	private String memberId;
	private int toolNo;
	private String toolName;
	private String toolLevel;
	private String toolPeriod;
	
	public String getMemberId() {
		return memberId;
	}
	public void setMemberId(String memberId) {
		this.memberId = memberId;
	}
	public int getToolNo() {
		return toolNo;
	}
	public void setToolNo(int toolNo) {
		this.toolNo = toolNo;
	}
	public String getToolName() {
		return toolName;
	}
	public void setToolName(String toolName) {
		this.toolName = toolName;
	}
	public String getToolLevel() {
		return toolLevel;
	}
	public void setToolLevel(String toolLevel) {
		this.toolLevel = toolLevel;
	}
	public String getToolPeriod() {
		return toolPeriod;
	}
	public void setToolPeriod(String toolPeriod) {
		this.toolPeriod = toolPeriod;
	}
	
	

}
