package com.dogether.vo;

public class LanguageVo {
 //사용언어
	private String memberId;
	private int languageNo;
	private String languageName;
	private String languageLevel;
	private String languagePeriod;
	
	public String getMemberId() {
		return memberId;
	}
	public void setMemberId(String memberId) {
		this.memberId = memberId;
	}
	public int getLanguageNo() {
		return languageNo;
	}
	public void setLanguageNo(int languageNo) {
		this.languageNo = languageNo;
	}
	public String getLanguageName() {
		return languageName;
	}
	public void setLanguageName(String languageName) {
		this.languageName = languageName;
	}
	public String getLanguageLevel() {
		return languageLevel;
	}
	public void setLanguageLevel(String languageLevel) {
		this.languageLevel = languageLevel;
	}
	public String getLanguagePeriod() {
		return languagePeriod;
	}
	public void setLanguagePeriod(String languagePeriod) {
		this.languagePeriod = languagePeriod;
	}
	
}
