package com.dogether.vo;

public class ProjectVo {
	
	private String memberId;
	private int projectNo;
	private String projectStartDate;
	private String projectEndDate;
	private int projectPeople;
	private String projectLocation;
	private String projectDetail;
	private String projectSubject;
	private String projectTech;
	private String projectEnrollStart;
	private String projectEnrollEnd;
	private int individualCompanyCheck;
	private int projectAmount;
	private int projectCheck;
	
	
	public int getProjectCheck() {
		return projectCheck;
	}
	public void setProjectCheck(int projectCheck) {
		this.projectCheck = projectCheck;
	}
	public String getMemberId() {
		return memberId;
	}
	public void setMemberId(String memberId) {
		this.memberId = memberId;
	}
	public int getProjectNo() {
		return projectNo;
	}
	public void setProjectNo(int projectNo) {
		this.projectNo = projectNo;
	}
	public String getProjectStartDate() {
		return projectStartDate;
	}
	public void setProjectStartDate(String projectStartDate) {
		this.projectStartDate = projectStartDate;
	}
	public String getProjectEndDate() {
		return projectEndDate;
	}
	public void setProjectEndDate(String projectEndDate) {
		this.projectEndDate = projectEndDate;
	}
	public int getProjectPeople() {
		return projectPeople;
	}
	public void setProjectPeople(int projectPeople) {
		this.projectPeople = projectPeople;
	}
	public String getProjectLocation() {
		return projectLocation;
	}
	public void setProjectLocation(String projectLocation) {
		this.projectLocation = projectLocation;
	}
	public String getProjectDetail() {
		return projectDetail;
	}
	public void setProjectDetail(String projectDetail) {
		this.projectDetail = projectDetail;
	}
	public int getProjectAmount() {
		return projectAmount;
	}
	public void setProjectAmount(int projectAmount) {
		this.projectAmount = projectAmount;
	}
	public String getProjectSubject() {
		return projectSubject;
	}
	public void setProjectSubject(String projectSubject) {
		this.projectSubject = projectSubject;
	}
	public String getProjectTech() {
		return projectTech;
	}
	public void setProjectTech(String projectTech) {
		this.projectTech = projectTech;
	}
	public String getProjectEnrollStart() {
		return projectEnrollStart;
	}
	public void setProjectEnrollStart(String projectEnrollStart) {
		this.projectEnrollStart = projectEnrollStart;
	}
	public String getProjectEnrollEnd() {
		return projectEnrollEnd;
	}
	public void setProjectEnrollEnd(String projectEnrollEnd) {
		this.projectEnrollEnd = projectEnrollEnd;
	}
	public int getIndividualCompanyCheck() {
		return individualCompanyCheck;
	}
	public void setIndividualCompanyCheck(int individualCompanyCheck) {
		this.individualCompanyCheck = individualCompanyCheck;
	}
	
	
	
}
