package com.dogether.vo;

public class PortfolioImageVo {
		
	private String memberId;
	private int portfolioNo;
	private String portfolioImageUrl;
	private String portfolioImageDate;
	
	public String getMemberId() {
		return memberId;
	}
	public void setMemberId(String memberId) {
		this.memberId = memberId;
	}
	public int getPortfolioNo() {
		return portfolioNo;
	}
	public void setPortfolioNo(int portfolioNo) {
		this.portfolioNo = portfolioNo;
	}
	public String getPortfolioImageUrl() {
		return portfolioImageUrl;
	}
	public void setPortfolioImageUrl(String portfolioImageUrl) {
		this.portfolioImageUrl = portfolioImageUrl;
	}
	public String getPortfolioImageDate() {
		return portfolioImageDate;
	}
	public void setPortfolioImageDate(String portfolioImageDate) {
		this.portfolioImageDate = portfolioImageDate;
	}
	
	

}
