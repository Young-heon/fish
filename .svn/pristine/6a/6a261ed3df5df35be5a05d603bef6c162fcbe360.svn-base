package com.dogether.dao;

import java.util.HashMap;
import java.util.List;

import com.dogether.vo.CareerVo;
import com.dogether.vo.CertificationVo;
import com.dogether.vo.LanguageVo;
import com.dogether.vo.MemberEnrollProjectVo;
import com.dogether.vo.MemberVo;
import com.dogether.vo.PortfolioVo;
import com.dogether.vo.ProjectVo;
import com.dogether.vo.ToolVo;

public interface MemberDao {
	
	int memberInsert(MemberVo vo);
	
	MemberVo memberIdCheck(String memberId);
	
	MemberVo memberLogin(HashMap<String,String> loginInfo);
	
	int projectInsert(ProjectVo vo);

	int memberImage(HashMap<String,String> imageUrl);
	
	String memberImageGet(String memberId);
	
	ProjectVo projectDetail(int projectNo);
	
	//프로젝트 페이징
	List<ProjectVo> projectPaging(int projectNo);
	
	//프리랜서 페이징
	List<MemberVo> memberPaging(HashMap<String,String> freeInfo);
	
	int socialLogin(HashMap<String,String> socialInfo);
	
	int socialCheck(HashMap<String,String> socialId);
	
	//이름 가져오는 부분
	String memberGetName(String memberId);
	
	//유저의 직군 코드 가져오는 부분
	String memberGetCheckCode(String memberId);
	
	//유저의 비밀번호 변경 기능
	int memberPasswordChange(HashMap<String,String> changePass);
	
	//유저의 핸드폰번호 변경 기능
	int memberPhoneChange(HashMap<String, String> changePhone);
	
	//유저의 주소 변경기능
	int memberAddrChange(HashMap<String, String> changeAddr);
	
	//신청한 유저를 해당 프로젝트에 등록
	int memberEnrollProject(HashMap<String,String> enrollMap);
	
	//유저가 신청한 프로젝트 정보 중복 확인하여 막는다.
	String memberEnrollProjectCheck(HashMap<String,String> enrollCheck);
	
	// 프리유저의 매칭전인 프로젝트 정보 가져온다.
	List <ProjectVo> userBeforeProject(String EnrollUser);
		
	// 프리유저의 진행중인 프로젝트 정보를 가져온다.
	List<ProjectVo> userOngoingProject(String EnrollUser);
	
	// 프리유저의 완료된 프로젝트 정보를 가져온다.
	List<ProjectVo> userDoneProject(String EnrollUser);
	
	//프로젝트 제공자의 매칭전 프로젝트 정보를 가져온다.
	List<ProjectVo> supplyBeforeProjectList(String memberId);
	
	
	//이력서 경력부분
	int careerInsert(CareerVo vo);
		
	List<CareerVo> careerSelect(String memberId);
		
	int careerDelete(int careerNo);

	//이력서 자격증부분
	int certificationInsert(CertificationVo vo);
	
	List<CertificationVo> certificationSelect(String memberId);
	
	int certificationDelete(int certificationNo);
	

	//이력서 사용언어부분
	int languageInsert(LanguageVo vo);
	
	List<LanguageVo> languageSelect(String memberId);
	
	int languageDelete(int languageNo);
	
	//이력서 디자인 툴 부분
	int toolInsert(ToolVo vo);
	
	List<ToolVo> toolSelect(String memberId);
	
	int toolDelete(int toolNo);
	
	//이력서 자기소개 부분
	int selfIntroUpdate(String memberId, String memberSelfIntro);
	
	String selfIntroSelect(String memberId);
	
	String getAthenCode(String memberId);
	
	//포트폴리오 부분
	int portfolioInsert(PortfolioVo vo);
	
	List<PortfolioVo>  portfolioSelect();
	
	int portfolioDelete(int portfolioNo);
	
	//포트폴리오 이미지 부분
	int portfolioImage(HashMap<String, String> imageUrl);
	 //이미지 주소가져오기
	String portfolioImageGet(HashMap<String,String>imageUrl);
	
	
	//프로젝트에 등록된 회원 인원 가져온다.
	String enrollProjectCount(String projectNo);
	
	//프로젝트에 등록된 회원 수를 업데이트한다.
	int updateProjectPeople(HashMap<String,String> updatePeople);
	
	//프로젝
	String projectGetImage(String projectNo);
	//개발자목록 이미지 
	String getDeveloperImage(String memberId);
	
	//프로젝트에 등록된 유저 리스트를 가져온다.
	
	List <MemberEnrollProjectVo> getProjectEnrollList(String memberId);
	
}
